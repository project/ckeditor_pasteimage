CKEDITOR.plugins.setLang( 'pasteimage', 'zh-cn', {
  dialogTitle: '文件上传提示',
  noticeTitle: '一些本地临时图片文件需要您手动上传。',
  fileListDescription: '请您先复制下方文件列表，点击确定后按 Ctrl + V 上传文件（每次最多10张）。',
});