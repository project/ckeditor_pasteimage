CKEDITOR.plugins.setLang('pasteimage', 'en', {
  dialogTitle: 'File Upload Notice',
  noticeTitle: 'Some local temp images needs you upload manualy.',
  fileListDescription: 'To paste the images, please copy the flowing file list and press Ctrl + V on the dialog that will open next(10 at most each time).',
});