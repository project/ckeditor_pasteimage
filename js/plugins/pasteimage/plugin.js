﻿(function () {
  'use strict';

  var uniqueNameCounter = 0,
          // Black rectangle which is shown before the image is loaded.
          loadingImage = 'data:image/gif;base64,R0lGODlhDgAOAIAAAAAAAP///yH5BAAAAAAALAAAAAAOAA4AAAIMhI+py+0Po5y02qsKADs=';

  var maps = [];

  // Returns number as a string. If a number has 1 digit only it returns it prefixed with an extra 0.
  function padNumber(input) {
    if (input <= 9) {
      input = '0' + input;
    }

    return String(input);
  }

  CKEDITOR.plugins.add('pasteimage', {
    requires: 'dialog',
    lang: 'en,zh-cn',
    init: init
  });

  function init(editor) {
    var fileTools = CKEDITOR.fileTools,
            uploadUrl = fileTools.getUploadUrl(editor.config, 'image');

    if (!uploadUrl) {
      return;
    }

    editor.addCommand('uploadlocalfiles', new CKEDITOR.dialogCommand('uploadlocalfiles'));
    CKEDITOR.dialog.add('uploadlocalfiles', function (api) {
      // CKEDITOR.dialog.definition
      var dialogDefinition = {
        title: editor.lang.pasteimage.dialogTitle,
        minWidth: 120,
        minHeight: 100,
        contents: [
          {
            id: 'tab1',
            label: 'Label',
            title: 'Title',
            expand: true,
            padding: 0,
            elements: [
              {
                type: 'html',
                html: '<p>' + editor.lang.pasteimage.noticeTitle + '</p><p>' + editor.lang.pasteimage.fileListDescription + '</p>'
              },
              {
                type: 'textarea',
                id: 'local_files_list',
                row: 10,
              },
              {
                type: 'file',
                id: 'local_selected_files',
                multiple: true,
                hidden: true,
                onChange: function (event) {
                  var tempDoc = document.implementation.createHTMLDocument(''),
                          temp = new CKEDITOR.dom.element(tempDoc.body),
                          imgs;

                  // Without this isReadOnly will not works properly.
                  temp.data('cke-editable', 1);

                  temp.appendHtml(editor.getData());

                  imgs = temp.find('img.cke-local-image');

                  var loaded = 0, total = Math.min(this.getInputElement().$.files.length, imgs.$.length);

                  for (var i = 0; i < this.getInputElement().$.files.length && i < imgs.$.length; i++) {

                    var file = this.getInputElement().$.files[i];
                    var reader = new FileReader();
                    reader.onload = (function () {
                      var img = imgs.getItem(i);
                      return function (evt) {
                        var imgSrc = evt.target.result;
                        img.setAttribute('src', imgSrc);
                        img.removeClass('cke-local-image');
                        var imgFormat = imgSrc.match(/image\/([a-z]+?);/i);
                        imgFormat = (imgFormat && imgFormat[ 1 ]) || 'jpg';
                        var loader = editor.uploadRepository.create(imgSrc, getUniqueImageFileName(imgFormat));
                        loader.upload(uploadUrl);

                        fileTools.markElement(img, 'uploadimage', loader.id);

                        fileTools.bindNotifications(editor, loader);

                        loaded++;

                        if (loaded >= total) {
                          editor.setData(temp.getHtml());
                        }

                      };
                    })();

                    reader.readAsDataURL(file);
                  }

                }
              }
            ]
          }
        ],
        buttons: [CKEDITOR.dialog.okButton],
        onShow: function () {
          var sep = "/";
          if (maps[0].indexOf(sep) < 0) {
            sep = "\\";
          }
          var parts = maps[0].split(sep);
          parts.pop();
          var dir = parts.join("\\");
          var path = maps.map(function (item) {
            parts = item.split(sep);
            return parts[parts.length - 1]
          });

          while (path.length > 10) {
            path.pop();
          }

          var textareaObj = this.getContentElement('tab1', 'local_files_list');
          textareaObj.setValue(dir + "\\" + ' "' + path.join('" "') + '"');
          textareaObj.select();
        },
        onOk: function () {
          var textareaObj = this.getContentElement('tab1', 'local_selected_files');
          textareaObj.getInputElement().$.multiple = true;
          textareaObj.getInputElement().$.accept = "image/*";
          textareaObj.getInputElement().$.click();
        }
      };

      return dialogDefinition;
    });
    editor.on('paste', function (evt) {
      // For performance reason do not parse data if it does not contain img tag and data attribute.
      if (!evt.data.dataValue.match(/<img[\s\S]+file:\/\/\//i)) {
        return;
      }

      var data = evt.data,
              // Prevent XSS attacks.
              tempDoc = document.implementation.createHTMLDocument(''),
              temp = new CKEDITOR.dom.element(tempDoc.body),
              imgs, img, i;

      // Without this isReadOnly will not works properly.
      temp.data('cke-editable', 1);

      temp.appendHtml(data.dataValue);

      imgs = temp.find('img');
      maps = [];

      for (i = 0; i < imgs.count(); i++) {
        img = imgs.getItem(i);

        // Assign src once, as it might be a big string, so there's no point in duplicating it all over the place.
        var imgSrc = img.getAttribute('src'),
                // Image have to contain src=data:...
                isDataInSrc = imgSrc && imgSrc.substring(0, 5) == 'file:',
                isRealObject = img.data('cke-realelement') === null;

        // We are not uploading images in non-editable blocs and fake objects (https://dev.ckeditor.com/ticket/13003).
        if (isDataInSrc && isRealObject && !img.data('cke-upload-id') && !img.isReadOnly(1)) {
          // Note that normally we'd extract this logic into a separate function, but we should not duplicate this string, as it might
          // be large.
          var imgFormat = imgSrc.match(/\.([a-z]+?)$/i),
                  loader;

          imgFormat = (imgFormat && imgFormat[ 1 ]) || 'jpg';

          maps.push(imgSrc.substring(8));
          img.addClass('cke-local-image');

        }
      }
      if (maps.length > 0) {
        editor.execCommand('uploadlocalfiles');
      }

      data.dataValue = temp.getHtml();
    });
  }
  // Returns a unique image file name.
  function getUniqueImageFileName(type) {
    var date = new Date(),
            dateParts = [date.getFullYear(), date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()];

    uniqueNameCounter += 1;

    return 'image_local-' + CKEDITOR.tools.array.map(dateParts, padNumber).join('') + '-' + uniqueNameCounter + '.' + type;
  }

})();
